﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Scale : Transition {
	// RectTransform of the transition canvas
	private RectTransform rectTransCanvasTransition;
	//Reference resolution from the canvasScaler (Scale with screen size)
	public Vector2 referenceResolution;

	//The size of the screen
	private Vector2 screenSize;


	public void Start()
	{
		//Assign the transitionCanvas
		transitionCanvas = GetComponent<AdminCanvas>().canvasTransition;

		//Set the rectTrans from the transition canvas, the screenSize and set reference resolution from the canvasScaler
		rectTransCanvasTransition = transitionCanvas.GetComponent<RectTransform>();
		screenSize.x = Screen.width;
		screenSize.y = Screen.height;
		
		//Reference resolution
		referenceResolution = GetComponent<CanvasScaler>().referenceResolution;
		
		//assign the screenCanvas array from the AdminCanvas
		screensCanvas = GetComponent<AdminCanvas>().screens;
		
		//If it has a menu, assign the menu
		if(GetComponent<AdminCanvas>().hasMenuCanvas)
			menuCanvas = GetComponent<AdminCanvas>().menuCanvas;
	}

	//Start transition of ScaleIn without loading
	public void ScaleTransition(int nextIndex, float timeInSec){
		//Update the new Index
		index = nextIndex;

		//assign the nextCanvas and the previousCanavas
		previousCanvas = screensCanvas[AdminCanvas.activeScreen];
		nextCanvas = screensCanvas[index];

		//Set the sortingOrder of the different Canvas
		previousCanvas.sortingOrder = 1;
		transitionCanvas.sortingOrder = 3;
		nextCanvas.sortingOrder = 1;

		if(menuCanvas != null)
			menuCanvas.sortingOrder = 2;

		//Start the courutine
		StartCoroutine ( ScaleIn (timeInSec, false));
	}


	//Start transition of ScaleIn with loading
	public void ScaleLoadingTransitionIn(float timeInSec){
		
		//Set the sortingOrder of the different Canvas
		screensCanvas[index].sortingOrder = 1;
		transitionCanvas.sortingOrder = 3;

		if(menuCanvas != null)
			menuCanvas.sortingOrder = 2;

		StartCoroutine ( ScaleIn (timeInSec,true));
	}

	//Start transition of FadeOut
	public void ScaleLoadingTransitionOut(float timeInSec){
		StartCoroutine ( FadeOut (timeInSec));
	}

	//Animation of the transitionCanvas scale
	private IEnumerator ScaleIn (float timeInSec, bool loading)
	{
		//Reset the rotation and the pivot
		Quaternion rotationCanvas = new Quaternion(0,0,0,0); 
		rectTransCanvasTransition.rotation = rotationCanvas;
		rectTransCanvasTransition.pivot = new Vector2(0.5f, 0.5f);

		//the leftOffsetStart is the position in X of the mouse in relation to the referenceResolution
		//The two OffsetStarts should add to the referenceResolution in X
		float leftOffsetStart = (Input.mousePosition.x /screenSize.x) * referenceResolution.x;
		float rightOffsetStart = referenceResolution.x - leftOffsetStart;

		//the bottomOffsetStart is the position in Y of the mouse in relation to the referenceResolution
		//The two OffsetStarts should add to the referenceResolution in Y
		float bottomOffsetStart = (Input.mousePosition.y /screenSize.y) * referenceResolution.y;
		float topOffsetStart = referenceResolution.y -bottomOffsetStart;

		//Set the offsets from the rectTransform of the CanvasTransition
		rectTransCanvasTransition.offsetMin = new Vector2(leftOffsetStart,bottomOffsetStart);
		rectTransCanvasTransition.offsetMax = new Vector2(-rightOffsetStart,-topOffsetStart);

		//Enable the transitionCanvas, make it visible and put it over the other canvas
		transitionCanvas.enabled = true;
		transitionCanvas.GetComponent<CanvasGroup>().alpha = 1;

		//Variable we use to make sure the tansition take a specific time
		float timeTransition = 0f;

		//Values of the offsets that will be alter over time
		float leftOffset;
		float rightOffset;
		float bottomOffset;
		float topOffset;

		while ( timeTransition < 1.0 )
		{
			//Add the deltaTime to the timeTransition in a way it takes half of the timeInSec
			timeTransition += Time.deltaTime * (2/timeInSec);

			//the Offsets will be smaller every time
			leftOffset = leftOffsetStart * (1-timeTransition);
			rightOffset = rightOffsetStart * (1-timeTransition);
			bottomOffset = bottomOffsetStart * (1-timeTransition);
			topOffset = topOffsetStart * (1-timeTransition);

			//We set the current Offsets
			rectTransCanvasTransition.offsetMin = new Vector2(leftOffset,bottomOffset);
			rectTransCanvasTransition.offsetMax = new Vector2(-rightOffset,-topOffset);
			yield return null;
		}


		//Check if the function is a loading transition
		if(!loading){

			//Enable the index canvas and disable all other canvas
			for (int i=0; i<screensCanvas.Length; i++)
			{
				if (i != index)
				{
					screensCanvas [i].enabled = false;
				}else{
					screensCanvas [i].enabled = true;
					screensCanvas [i].transform.localPosition = new Vector3 (0, 0, 0);
				}
			}

			//Start the FadeOut Coroutine
		 	yield return StartCoroutine(FadeOut (timeInSec));
			
			//Update the active screen
			AdminCanvas.activeScreen = index;

		}else{
			//Activate the Loading animation
			transitionCanvas.GetComponent<LoadingTransition>().ActivateLoading();
		}

	}

	//Fade out of the transitionCanvas
	private IEnumerator FadeOut(float timeInSec){
		

		//Reset the timeTransition
		float timeTransition = 0;
		while ( timeTransition < 1.0 )
		{
			//Add the deltaTime to the timeTransition in a way it takes half of the timeInSec
			timeTransition += Time.deltaTime * (2/timeInSec);
			//Make the alpha smaller every time
			transitionCanvas.GetComponent<CanvasGroup>().alpha = 1 - timeTransition;
			yield return null;
		}

		//Disable the transitionCanvas
		transitionCanvas.enabled = false;
		//chage the inTransition variable to false
		AdminCanvas.inTransition = false;
		}


}
