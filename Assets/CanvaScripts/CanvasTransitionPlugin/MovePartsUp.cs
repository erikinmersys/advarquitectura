﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePartsUp : Transition {

	// Use this for initialization
	void Start () {
		//Set the canvas array
		screensCanvas = GetComponent<AdminCanvas>().screens;

		//If it has a menu, assign the menu
		if(GetComponent<AdminCanvas>().hasMenuCanvas)
			menuCanvas = GetComponent<AdminCanvas>().menuCanvas;
	}

	public void MoveUpTransition(int index, float timeInSec){

		//Set the nextCanvas and the previousCanavas
		previousCanvas = screensCanvas[AdminCanvas.activeScreen];
		nextCanvas = screensCanvas[index];

		//Set the sortingOrder of the different Canvas
		previousCanvas.sortingOrder = 0;
		nextCanvas.sortingOrder = 1;

		if(menuCanvas!=null)
			menuCanvas.sortingOrder = 2;

		//Move the next canvas to the center of the screen
		nextCanvas.transform.localPosition = new Vector3 (0, 0, 0);

		//Move the parts of the nextCanvas out of the view
		nextCanvas.GetComponent<CanvasPartsUp>().MoveDownOut();

		//Make the naxtCanvas transparent 
		nextCanvas.GetComponent<CanvasGroup>().alpha = 0;

		//Activate the nextCanvas
		nextCanvas.enabled = true;

		//Start courutine
		StartCoroutine ( Fade (index, timeInSec));
	}

	private IEnumerator Fade (int index, float timeInSec)
	{
		//First movement Fade
		float timeTransition = 0;
		while ( timeTransition < 1.0 )
		{
			//Add the deltaTime to the timeTransition in a way it takes half of the timeInSec
			timeTransition += Time.deltaTime * (2/timeInSec);

			//Make the canvas visible
			nextCanvas.GetComponent<CanvasGroup>().alpha = timeTransition;
			yield return null;
		}

		//After the transitionCanvas has cover the whole screen, we enable the index canvas and disable all other canvas
		for (int i=0; i<screensCanvas.Length; i++)
		{
			if (i != index)
			{
				screensCanvas [i].enabled = false;
			}else{
				screensCanvas [i].enabled = true;
			}
		}

		//Move up the objects of the Canvas
		nextCanvas.GetComponent<CanvasPartsUp>().MoveUpIn(index, timeInSec);
	}
}
