﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Roll : Transition {
	//RecTransform of the transitionCAnvas
	private RectTransform transformCanvas;
	

	// Use this for initialization
	void Start () {
		//Set the screenCanvas and the transitionCanvas from the AdminCanvas
		screensCanvas = GetComponent<AdminCanvas>().screens;
		transitionCanvas = GetComponent<AdminCanvas>().canvasTransition;
		
		//Set the rectTrans from the transitionCanvas
		transformCanvas = transitionCanvas.GetComponent<RectTransform>();

		//If it has a menu, assign the menu
		if(GetComponent<AdminCanvas>().hasMenuCanvas)
			menuCanvas = GetComponent<AdminCanvas>().menuCanvas;
	}

	//Start transition of rollIn without loading
	public void RollTransition(int newIndex, float timeInSec){
		
		//Update the new Index
		index = newIndex;

		//Set the nextCanvas and the previousCanavas
		previousCanvas = screensCanvas[AdminCanvas.activeScreen];
		nextCanvas = screensCanvas[index];

		//Set the sortingOrder of the different Canvas
		previousCanvas.sortingOrder = 0;
		transitionCanvas.sortingOrder = 2;
		nextCanvas.sortingOrder = 0;

		if(menuCanvas!=null)
			menuCanvas.sortingOrder = 1;

		//Start coroutine RollIn
		StartCoroutine ( RollIn (timeInSec,false));
	}

	//Start transition of rollIn with loading
	public void RollLoadingTransitionIn(float timeInSec){

		//Set the sortingOrder of the different Canvas
		screensCanvas[index].sortingOrder = 0;
		transitionCanvas.sortingOrder = 2;

		if(menuCanvas!=null)
			menuCanvas.sortingOrder = 1;

		StartCoroutine ( RollIn (timeInSec,true));
	}

	//Start transition of rollOut 
	public void RollLoadingTransitionOut(float timeInSec){
		StartCoroutine ( RollOut (timeInSec));
	}

	private IEnumerator RollIn (float timeInSec, bool loading){
		//Chage the rotation of the transitionCanvas
		Vector3 rotationCanvas = new Vector3(0,0,90); 
		transformCanvas.localEulerAngles = rotationCanvas;

		//Chage the pivot position from the transitionCanvas
		transformCanvas.pivot = new Vector2(0f, 0f);

		//Move the transitionCanvas
		transformCanvas.offsetMin = new Vector2(0,0);
		transformCanvas.offsetMax = new Vector2(0,0);

		//Enable the transitionCanvas and put it over the other canvas
		transitionCanvas.enabled = true;
		transitionCanvas.sortingOrder = 1;

		//Time we use to make sure the tansition take a specific time
		float timeTransition = 1f;
		
		//TransitionCanvas rolls in
	    while ( timeTransition > 0 )
    	{
		//Add the deltaTime to the timeTransition in a way it takes a third of the timeInSec
		timeTransition -= Time.deltaTime * (3/timeInSec);

		//We rotate the transitionCanvas to make it roll in
		rotationCanvas = new Vector3(0,0,90*Mathf.Clamp(timeTransition, 0, 1));
		transformCanvas.localEulerAngles = rotationCanvas;
		
		//We add some transparency
		float alphaValue = Mathf.Clamp(1 - timeTransition, 0.5f, 1);
		transitionCanvas.GetComponent<CanvasGroup>().alpha = alphaValue;

		yield return null;
		}
				

		//Check if the function is a loading transition
		if(!loading){

			//Enable the index canvas and disable all other canvas
			for (int i=0; i<screensCanvas.Length; i++)
			{
				if (i != index)
				{
					screensCanvas [i].enabled = false;
				}else{
					screensCanvas [i].enabled = true;
					screensCanvas [i].transform.localPosition = new Vector3 (0, 0, 0);
				}
			}

			//We make the transition canvas stays in the whole screen for a moment (one third of the timeInSec)
			timeTransition = 1;

			while ( timeTransition > 0 )
			{
				//Add the deltaTime to the timeTransition in a way it takes a third of the timeInSec
				timeTransition -= Time.deltaTime * (3/timeInSec);
				yield return null;
			}

			//Start the RollOut Coroutine
			yield return StartCoroutine(RollOut (timeInSec));

			//Update the active screen
			AdminCanvas.activeScreen = index;

		}else{
			//Activate the Loading animation
			transitionCanvas.GetComponent<LoadingTransition>().ActivateLoading();
		}
	}
	private IEnumerator RollOut (float timeInSec){
		
		//Chage the pivot position from the transitionCanvas
		transformCanvas.pivot = new Vector2(1f, 0f);

		//Time we use to make sure the tansition take a specific time
		float timeTransition = 0f;

		//TransitionCanvas rolls out
	    while ( timeTransition < 1.0f )
    	{
		//Add the deltaTime to the timeTransition in a way it takes half of the timeInSec
		timeTransition += Time.deltaTime * (3/timeInSec);

		//We rotate the transitionCanvas to make it roll in
		Vector3 rotationCanvas = new Vector3(0,0,-90*Mathf.Clamp(timeTransition, 0, 1));
		transformCanvas.localEulerAngles = rotationCanvas;
		
		//We add some transparency
		float alphaValue = Mathf.Clamp(1 - timeTransition, 0.5f, 1);
		transitionCanvas.GetComponent<CanvasGroup>().alpha = alphaValue;
		

		yield return null;
		}

		//Disable the transitionCanvas
		transitionCanvas.enabled = false;
		//chage the inTransition variable to false
		AdminCanvas.inTransition = false;

		//Chage the pivot position from the transitionCanvas
		transformCanvas.pivot = new Vector2(0.5f, 0.5f);
		//We rotate the transitionCanvas to make it roll in
		Vector3 rotationCanvasFinal = new Vector3(0,0,0);
		transformCanvas.localEulerAngles = rotationCanvasFinal;
		
	}

	
}