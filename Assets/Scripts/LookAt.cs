﻿// ************************************************************************ 
// File Name:   LookAt.cs 
// Purpose:    	Object look at target
// Project:		ADVArquitecture
// Author:      Erik Marmolejo Ensastegui
// Copyright: 	2020 Inmersys
// ************************************************************************ 

// ************************************************************************ 
// Imports 
// ************************************************************************ 
using System.Collections.Generic;
using UnityEngine;

public class LookAt : MonoBehaviour
{
    Transform target;   //Target del lookat

    private void Start()
    {
        SetTarget(ModelManager.instance.main.transform); //Inicialmente el target es la camara principal
    }

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(target);

        List<Touch> touches = InputHelper.GetTouches();
        if ((touches.Count > 0) && (touches[0].phase == TouchPhase.Began))
        {
            Ray raycast = target.GetComponent<Camera>().ScreenPointToRay(touches[0].position);
            RaycastHit raycastHit;
            if (Physics.Raycast(raycast, out raycastHit))
            {
                if (raycastHit.collider.CompareTag("EtiquetaInterior"))
                {
                    print(raycastHit.collider.name);
                    ModelManager.instance.interiores.material.mainTexture = ModelManager.instance.GetTexture(raycastHit.collider.name);
                    //Si se toca la etiqueta para ver interiores abre el pop up que corresponda ya sea con RA o sin RA(WORA)
                    if (TransitionControl.instance.curModelActive.transform.parent.parent.name == "WORA")
                        TransitionControl.instance.OpenPopUpInteriorWORA();
                    else
                        TransitionControl.instance.OpenPopUpInterior();
                    //TODO: para realizar el llamado a determinada textura de interiores nombrar con un id a
                    //      la etiqueta y enviar como parametro de estas funciones dicho id (name) para
                    //      asignar la textura correspondiente a la escena
                }
                else if(raycastHit.collider.CompareTag("EtiquetaImagen"))
                {
                    if (TransitionControl.instance.curModelActive.transform.parent.parent.name == "WORA")
                        TransitionControl.instance.panelFullEtiquetaWORA.SetActive(true);
                    else
                        TransitionControl.instance.panelFullEtiqueta.SetActive(true);
                }
            }
        }
    }

    //Cambia el target
    public void SetTarget(Transform target)
    {
        this.target = target;
    }
}
