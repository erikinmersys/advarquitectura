﻿// ************************************************************************ 
// File Name:   ModelManager.cs 
// Purpose:    	All models manager
// Project:		ADVArquitecture
// Author:      Erik Marmolejo Ensastegui
// Copyright: 	2020 Inmersys
// ************************************************************************ 

// ************************************************************************ 
// Imports 
// ************************************************************************ 
using System;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class ModelManager : Singleton<ModelManager>
{
    #region public vars
    public Camera main;
    public GameObject builds;
    public List<GameObject> buildModels;
    public List<DesarrolloInfo> desarrollos;
    public Transform parentDesarrollos;
    public Transform parentCamera;
    public Transform parentModelWora;
    public GameObject gestureWORA;
    public GameObject desarrollo;
    public GameObject imageSlide;
    public VuforiaBehaviour RA;
    public GameObject Interiores;
    public Transform parentTemp;
    public MeshRenderer interiores;
    public List<Texture> interioresRenders;
    #endregion //public vars

    #region private methods
    // Start is called before the first frame update
    void Start()
    {
        //De la lista de desarrollos crea objetos que serán instanciados en la pantalla principal con toda su información
        for(int x = 0; x< desarrollos.Count; x++)
        {
            GameObject curdesarrollo = Instantiate(desarrollo, parentDesarrollos);
            Desarrollo desa = curdesarrollo.GetComponent<Desarrollo>();
            desa.index = x;
            desa.previeDesarrolloImg.sprite = desarrollos[x].previewDesarrollo;
            desa.nViviendasTxt.text = desarrollos[x].nViviendas;
            desa.nombreTxt.text = desarrollos[x].nombre;
            desa.tipoTxt.text = desarrollos[x].tipo;
        }
    }
    #endregion //private methods

    #region public methodss
    /// <summary>
    /// Cambia el objeto padre de los modelos de desarrollo ya que pueden estar en RA o sin RA y define su localposition
    /// </summary>
    /// <param name="parent"> el objeto que ahora será el objeto padre</param>
    public void SetParentBuild(Transform parent)
    {
        builds.transform.SetParent(parent);
            builds.transform.localPosition = Vector3.zero;
            builds.transform.localEulerAngles = new Vector3(0, 0, 0);
            /*foreach(Transform child in builds.transform)
            {
                child.localEulerAngles = new Vector3(0, 180, 0);
            }*/

    }

    public Texture GetTexture(string name)
    {
        switch (name)
        {
            case "depaBret":
                return interioresRenders[0];
            case "roofBret":
                return interioresRenders[1];
            case "depaCuau":
                return interioresRenders[2];
            case "roofCuau":
                return interioresRenders[3];
        }
        return null;
    }
    #endregion //public methods
}
//Clase serializable que contiene toda la información de los desarrollos que se utilizará a lo largo de la app
[Serializable]
public class DesarrolloInfo 
{
    public List<Sprite> carrusel;
    public List<Sprite> plantas;
    public Sprite previewDesarrollo;
    public string nViviendas;
    public string nombre;
    public string tipo;
    public string descripcion;
    public string locationURL;
}
