﻿// ************************************************************************ 
// File Name:   TransitionControl.cs 
// Purpose:    	Control all transition animation
// Project:		ADVArquitecture
// Author:      Erik Marmolejo Ensastegui
// Copyright: 	2020 Inmersys
// ************************************************************************ 

// ************************************************************************ 
// Imports 
// ************************************************************************ 
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

// ************************************************************************ 
// Class: TransitionControl
// ************************************************************************ 
public class TransitionControl : Singleton<TransitionControl>
{
    #region public vars
    public int curDesarrollo;       //id del desarrollo seleccionado actualmente
    public List<Image> circulitos;  //Lista dinámica para los circulitos del carrusel de renders
    public Transform parentCirculitos;      //objeto padre de los circulitos
    public GameObject imageCirculito;   //El objeto circulito que se instancia uno por cada imagen de render
    public GameObject slideShowRoot;    //El prefab del carrusel que se instancía
    public GameObject slideShow;        //Referencia al objeto ya instanciado del slideshow
    public GameObject fullImage;
    public Transform parentSlide;   //Objeto padre del slide show instanciado
    public TextMeshProUGUI titleMaqueta;    //Texto para el titulo del desarrollo en la maqueta
    //public GameObject builds;
    public string curLocationDesarrollo;    //Dirección del desarrollo seleccionado
    public GameObject flashDescarga;        //Imágen que aparece y desaparece cuando se guarda una imagen
    public GameObject mensajeDescarga;      //Objeto que contiene el mensaje de imagen guardada
    public GameObject panelFullImageCarrusel0;
    public GameObject panelFullImageCarrusel1;
    public Image imageFullCarrusel;
    #endregion //public vars

    #region private vars
    private bool tutoMaqueta;       //Define si ya se vio el tuto de maqueta virtual
    private bool tutoPlantas;
    #endregion //private vars

    #region private methods
    #endregion  //private methods

    #region public methods
    //LLama la animación de entrar de abajo a arriba
    public void EntraObjetoAbajoArriba(GameObject objeto)
    {
        var anim = objeto.GetComponent<Animator>();     //Obtiene el componente animator
        anim.SetTrigger("EntraAbajoArriba");        //Lanza el trigger para activar la animación
    }

    //LLama la animación de salir de arriba a abajo
    public void SaleObjetoArribaAbajo(GameObject objeto)
    {
        var anim = objeto.GetComponent<Animator>();     //Obtiene el componente animator
        anim.SetTrigger("SaleArribaAbajo");     //Lanza el trigger para activar la animación
    }

    //Proceso para instanciar todo lo que corresponde al desarrollo seleccionado
    public void GoSelectedDesarrollo(int index)
    {
        if (index >= 0) //Si el inde es mayor o igual a cero
            curDesarrollo = index;  //Se almacena en esta variable
        slideShow = Instantiate(slideShowRoot, parentSlide);    //Instanciamos el slideshow y creamos la referencia
        parentCirculitos = slideShow.transform.Find("Image");   //Guardamos el parentCirculitos que esta en el slideShow
        FindObjectOfType<DesarrolloSeleccionado>().parentCarrusel = slideShow.transform.Find("Background"); //Se asigna el paren del slideShow
        FindObjectOfType<AdminCanvas>().ChangeCanvas(2);    //Se realiza el cambio de Canvas
        DesarrolloSeleccionado.instance.title.text = ModelManager.instance.desarrollos[curDesarrollo].nombre;       //Se asigna el texto del nombre
        DesarrolloSeleccionado.instance.descripcion.text = ModelManager.instance.desarrollos[curDesarrollo].descripcion;    //Se asigna el texto de la descripción
        DesarrolloSeleccionado.instance.carrusel = ModelManager.instance.desarrollos[curDesarrollo].carrusel;   //Se asignan las imagenes del slideshow
        curLocationDesarrollo = ModelManager.instance.desarrollos[curDesarrollo].locationURL;   //Se asigna la ubicación
        int indexImg = 0;   //El index de la imagen empieza en 0
        if (circulitos == null) //Si circulitos es null
            circulitos = new List<Image>(); //Se crea nueva lista
        else   //Si no
            circulitos.Clear(); //Se limpia
        foreach (Transform cir in parentCirculitos) //Eliminamos cada circulito creado previamente
            Destroy(cir.gameObject);
        foreach (var img in DesarrolloSeleccionado.instance.carrusel)       //Por cada imagen en el carrusel
        {
            GameObject curimgdesarrollo = Instantiate(ModelManager.instance.imageSlide, DesarrolloSeleccionado.instance.parentCarrusel);    //Se instancia cada imagen del carrusel
            curimgdesarrollo.GetComponent<Image>().sprite = img;    //Se le asiga su imagen
            curimgdesarrollo.GetComponent<SelectImage>().index = indexImg;  //Se le asigna un index de imagen
            //circulitos[indexImg].gameObject.SetActive(true);
            //GameObject circulito = new GameObject();
            GameObject circulito = Instantiate(imageCirculito, parentCirculitos);   //A cada imagen le corresponde un circulito
            circulitos.Add(circulito.GetComponent<Image>());    //El circulito se agrega a la lista de circulitos
            if (indexImg == 0)  //Si el index de imagen es cero
                circulito.transform.GetChild(0).gameObject.SetActive(true); //Se activa
            else  //Si no
                circulito.transform.GetChild(0).gameObject.SetActive(false);    //Se desactiva
            indexImg++; //Pasa al siguiente index de imagen
        }
        UISlideShow.SP.InitialStartSlide(); //Despues de todas las imagenes se inicia el slideshow
    }

    public Texture2D myTexture;     //Imagen que contiene el marcador para ser descargado

    //Se llama para guardar en galeria al marcador
    public void DescargaTarget()
    {
        NativeGallery.SaveImageToGallery(myTexture, "ADVArquitectura", "TargetADV.jpg");    //Guarda la imagen en la galeria
        StartCoroutine(FlashDescargar());   //Llama la corutina de feedback de que se guardo la imagen
    }

    public GameObject popUpGoToMapas;   //PopUp de mensaje para ir a ver mapas

    //Llama el mensaje de ver mapas
    public void GoToMapsLocation()
    {
        popUpGoToMapas.SetActive(true); //Activa el popup
        popUpGoToMapas.GetComponent<Animator>().SetTrigger("CreceCeroUno"); //Inicia animacion de crecer de cero a uno
    }

    //Confirmación del mensaje de ir a ver mapas
    public void SiMapa()
    {
        popUpGoToMapas.GetComponent<Animator>().SetTrigger("DecreceUnoCero");   //Inicia animacion de decrece de uno a cero
        GetComponent<AdminCanvas>().AbreLinkExterno(curLocationDesarrollo); //Manda llamar los mapas con la ubicación
    }

    //Cancela y sólo desaparece el mensaje
    public void NoMapa()    
    {
        popUpGoToMapas.GetComponent<Animator>().SetTrigger("DecreceUnoCero");
    }

    //*******Objetos que corresponden a los que aparecen en el tutorial y pantalla de la maqueta virtual
    public GameObject texto1;
    public GameObject texto2;
    public GameObject texto3;
    public GameObject objeto1;
    public GameObject objeto2;
    public GameObject objeto3;
    public GameObject botonMarcador;
    public GameObject botonFoto;
    public GameObject footerMV;
    public GameObject btnBack;
    public GameObject fondo;
    public Button btnWora;
    public Image screenShot;
    public GameObject PanelSS;
    public GameObject popUpConfirmarDescartar;
    public GameObject panelFullEtiqueta;
    //*********************************************************************************************************
    Texture2D ss;   //Variable donde se guarda la captura de pantalla

    //Activa y desactiva los objetos correspondientes a la primera parte del Tuto (La de descarga del marcador)
    public void StartTuto()
    {
        //ModelManager.instance.RA.SetActive(true);
        ModelManager.instance.main.enabled = true;
        ModelManager.instance.RA.enabled = true;
        texto2.SetActive(false);
        texto3.SetActive(false);
        texto1.SetActive(true);
        objeto2.SetActive(false);
        objeto3.SetActive(false);
        objeto1.SetActive(true);
        texto1.GetComponent<Animator>().SetBool("FadeTextBool", true);
    }

    //Inicia la segunda parte del tutorial
    public void ContinuaTuto()
    {
        if (!tutoMaqueta)
            StartCoroutine(continuaTuto());
        else
        {
            tutoMaqueta = true;
            botonMarcador.SetActive(false);
            botonFoto.SetActive(true);
            objeto1.SetActive(false);
            texto1.SetActive(false);
            objeto2.SetActive(false);
            texto2.SetActive(false);
            fondo.SetActive(false);
            texto3.SetActive(false);
            objeto3.SetActive(false);
        }
    }

    //Activa y desactiva objetos y animaciones en el tiempo correspondiente para el buen funcionamiento del tutorial
    IEnumerator continuaTuto()
    {
        fondo.SetActive(true);
        yield return new WaitForSeconds(1);
        botonMarcador.SetActive(false);
        botonFoto.SetActive(true);
        objeto1.SetActive(false);
        objeto2.SetActive(true);
        texto1.SetActive(false);
        texto2.SetActive(true);
        texto2.GetComponent<Animator>().SetBool("FadeTextBool", true);
        objeto2.GetComponent<Animator>().SetBool("Resize", true);
        yield return new WaitForSeconds(6);
        objeto2.SetActive(false);
        //objeto3.SetActive(true);
        texto2.SetActive(false);
        //texto3.SetActive(true);
        //texto3.GetComponent<Animator>().SetBool("FadeTextBool", true);
        //objeto3.GetComponent<Animator>().SetBool("Rotar", true);
        //yield return new WaitForSeconds(6);
        fondo.SetActive(false);
        texto3.SetActive(false);
        objeto3.SetActive(false);
    }

    //Llama al proceso para cerrar la maqueta virtual
    public void CerrarMaquetaVir()
    {
        StartCoroutine(cerrarMaquetaVir());
    }

    //Se colocan los objetos en el status correcto para cerrar la maqueta y abrirla nuevamente si se desea
    IEnumerator cerrarMaquetaVir()
    {
        comeFrom360 = false;
        comeFromWora = true;
        botonMarcador.SetActive(true);
        botonFoto.SetActive(false);
        objeto1.SetActive(false);
        texto1.SetActive(false);
        texto2.GetComponent<Animator>().SetBool("FadeTextBool", false);
        objeto2.GetComponent<Animator>().SetBool("Resize", false);
        objeto2.SetActive(false);
        texto2.SetActive(false);
        texto3.GetComponent<Animator>().SetBool("FadeTextBool", false);
        objeto3.GetComponent<Animator>().SetBool("Rotar", false);
        fondo.SetActive(true);
        texto3.SetActive(false);
        objeto3.SetActive(false);
        ModelManager.instance.SetParentBuild(ModelManager.instance.parentTemp);
        yield return new WaitForSeconds(0.1f);
        //ModelManager.instance.RA.SetActive(false);
        ModelManager.instance.main.enabled = false;
        ModelManager.instance.RA.enabled = false;
        StopAllCoroutines();    //Detiene las corutinas actuales para que nada continue si no es necesario
    }

    //Detiene el tuto si esta corriendo y regresa los estados de los objetos a los correspondientes
    public void OmitirTutoMaqueta()
    {
        StopAllCoroutines();
        botonMarcador.SetActive(false);
        botonFoto.SetActive(true);
        objeto1.SetActive(false);
        texto1.SetActive(false);
        texto2.GetComponent<Animator>().SetBool("FadeTextBool", false);
        objeto2.GetComponent<Animator>().SetBool("Resize", false);
        objeto2.SetActive(false);
        texto2.SetActive(false);
        texto3.GetComponent<Animator>().SetBool("FadeTextBool", false);
        objeto3.GetComponent<Animator>().SetBool("Rotar", false);
        fondo.SetActive(false);
        texto3.SetActive(false);
        objeto3.SetActive(false);
    }

    public void OmitirTutoMaqueta2()
    {
        botonMarcador.SetActive(false);
        botonFoto.SetActive(true);
        objeto1.SetActive(false);
        texto1.SetActive(false);
        texto2.GetComponent<Animator>().SetBool("FadeTextBool", false);
        objeto2.GetComponent<Animator>().SetBool("Resize", false);
        objeto2.SetActive(false);
        texto2.SetActive(false);
        texto3.GetComponent<Animator>().SetBool("FadeTextBool", false);
        objeto3.GetComponent<Animator>().SetBool("Rotar", false);
        fondo.SetActive(false);
        texto3.SetActive(false);
        objeto3.SetActive(false);
    }

    //Llama el proceso para hacer una captura de pantalla
    public void TomaFoto()
    {
        StartCoroutine(TakeScreenshotAndPut());
    }

    //Lo correspondiente a la captura de pantalla
    private IEnumerator TakeScreenshotAndPut()
    {
        //***Primero se oculta todo para que no aparezca en la captura
        btnBack.SetActive(false);
        btnWora.gameObject.SetActive(false);
        footerMV.SetActive(false);
        botonFoto.SetActive(false);
        yield return new WaitForEndOfFrame();
        //************************************************************
        //*****Se toma la captura de pantalla y se abre el panel que contiene botones para interactuar con ella
        ss = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        ss.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        ss.Apply();
        screenShot.sprite = Sprite.Create(ss, new Rect(0, 0, ss.width, ss.height), new Vector2(0.5f, 0.5f));
        PanelSS.SetActive(true);
        yield return new WaitForEndOfFrame();
        btnBack.SetActive(true);
        btnWora.gameObject.SetActive(true);
        footerMV.SetActive(true);
        botonFoto.SetActive(true);
        //******************************************************************************************************
    }

    //Proceso de compartir la captura
    public void Compartir()
    {
        string filePath = Path.Combine(Application.temporaryCachePath, "advImage.png");
        File.WriteAllBytes(filePath, ss.EncodeToPNG());

        new NativeShare().AddFile(filePath)
            .SetSubject("ADV Arquitectura").SetText(ModelManager.instance.desarrollos[curDesarrollo].nombre)
            .SetCallback((result, shareTarget) => Debug.Log("Share result: " + result + ", selected app: " + shareTarget))
            .Share();

        // Share on WhatsApp only, if installed (Android only)
        //if( NativeShare.TargetExists( "com.whatsapp" ) )
        //	new NativeShare().AddFile( filePath ).AddTarget( "com.whatsapp" ).Share();
    }

    //Proceso para guardar la captura en la galeria
    public void Guardar()
    {
        // Save the screenshot to Gallery/Photos
        NativeGallery.Permission permission = NativeGallery.SaveImageToGallery(ss, "ADVArquitectura", ModelManager.instance.desarrollos[curDesarrollo].nombre + ".png", (success, path) => Debug.Log("Media save result: " + success + " " + path));
        StartCoroutine(FlashDescargar());
        StartCoroutine(siDescartar(2.2f));
        Debug.Log("Permission result: " + permission);
    }

    //Proceso para descartar la captura, llama un popup
    public void Descartar()
    {
        popUpConfirmarDescartar.SetActive(true);
        popUpConfirmarDescartar.GetComponent<Animator>().SetTrigger("CreceCeroUno");
    }

    //Llama el proceso de descartar
    public void SiDescartar()
    {
        StartCoroutine(siDescartar(1.0f));
    }

    //Apaga el popup de confirmación para descartar
    public void NoDescartar()
    {
        popUpConfirmarDescartar.GetComponent<Animator>().SetTrigger("DecreceUnoCero");
    }

    //Corutina para descartar
    IEnumerator siDescartar(float firstwait)
    {
        popUpConfirmarDescartar.GetComponent<Animator>().SetTrigger("DecreceUnoCero");  //Primero desaparece el popup
        yield return new WaitForSeconds(firstwait);     //Espera el tiempo establecido
        PanelSS.SetActive(false);   //Apaga el panel del ss
        Destroy(ss);    //Destruye la textura para no consumir memoria
    }

    //Proceso de feedback para imagen guardada
    IEnumerator FlashDescargar()
    {
        flashDescarga.SetActive(true);  //Aparece panel en blanco
        yield return new WaitForSeconds(0.1f);
        mensajeDescarga.SetActive(true);    //Aparece mensaje de guardado
        yield return new WaitForSeconds(0.1f);
        flashDescarga.SetActive(false); //Desaparece panel en blanco inmediatamente
        yield return new WaitForSeconds(2f);
        mensajeDescarga.SetActive(false);   //Despues de dos segundo desaparece mensaje
    }

    //****Objetos para la pantalla de plantas uncluyendo su tutorial
    public GameObject texto1Plantas;
    public GameObject texto2Plantas;
    public TextMeshProUGUI textoTitlePlantas;
    public GameObject objeto1Plantas;
    public GameObject objeto2Plantas;
    public GameObject fondoPlantas;
    public GesturePlantaBH plantas;
    public Image imgPlanta;
    int curPlanta = 0;
    //*************************************************************

    //Activa el script para manipular la planta
    public void EnableGesturesPlantas(bool enable)
    {
        plantas.enabled = enable;
    }

    //Hace el tamaño y posición default de la planta
    void SetIndexPlanta()
    {
        imgPlanta.transform.localScale = Vector3.one;
        imgPlanta.transform.localPosition = Vector3.zero;
        imgPlanta.sprite = ModelManager.instance.desarrollos[curDesarrollo].plantas[curPlanta];
        textoTitlePlantas.text = ModelManager.instance.desarrollos[curDesarrollo].plantas[curPlanta].name;
    }

    //Obtiene la planta correspondiente ya sea la siguiente o la previa y la asigna
    public void GetIndexPlanta(bool next)
    {
        if (next)
        {
            if (curPlanta < ModelManager.instance.desarrollos[curDesarrollo].plantas.Count - 1)
                curPlanta++;
            else
                curPlanta = 0;
        }
        else
        {
            if (curPlanta > 0)
                curPlanta--;
            else
                curPlanta = ModelManager.instance.desarrollos[curDesarrollo].plantas.Count - 1;
        }

        SetIndexPlanta();
    }

    //Proceso del tutorial de las plantas
    public void ContinuaTutoPlantas()
    {
        SetIndexPlanta();
        plantas.transform.localScale = Vector3.one;
        plantas.transform.localPosition = Vector3.zero;
        StartCoroutine(startTutoPlantas());
    }

    //Activa y desactiva objetos correspondientes al tutorial de plantas
    IEnumerator startTutoPlantas()
    {
        objeto1Plantas.SetActive(true);
        texto1Plantas.SetActive(true);
        texto1Plantas.GetComponent<Animator>().SetBool("FadeTextBool", true);
        objeto1Plantas.GetComponent<Animator>().SetBool("FadeIcon", true);
        yield return new WaitForSeconds(6);
        objeto2Plantas.SetActive(true);
        texto2Plantas.SetActive(true);
        objeto1Plantas.SetActive(false);
        texto1Plantas.SetActive(false);
        texto2Plantas.GetComponent<Animator>().SetBool("FadeTextBool", true);
        objeto2Plantas.GetComponent<Animator>().SetBool("Resize", true);
        yield return new WaitForSeconds(6);
        fondoPlantas.SetActive(false);
        texto2Plantas.SetActive(false);
        objeto2Plantas.SetActive(false);
        EnableGesturesPlantas(true);
    }

    //Regrea objetos a status default para cerrar plantas
    public void CerrarPlantas()
    {
        StopAllCoroutines();
        texto1Plantas.GetComponent<Animator>().SetBool("FadeTextBool", false);
        objeto1Plantas.GetComponent<Animator>().SetBool("FadeIcon", false);
        objeto1Plantas.SetActive(false);
        texto1Plantas.SetActive(false);
        texto2Plantas.GetComponent<Animator>().SetBool("FadeTextBool", false);
        objeto2Plantas.GetComponent<Animator>().SetBool("Resize", false);
        fondoPlantas.SetActive(true);
        texto2Plantas.SetActive(false);
        objeto2Plantas.SetActive(false);
        EnableGesturesPlantas(false);
    }

    //Interrumpe tutorial si esta corriendo y regresa todo a estatus default
    public void OmitirTutoPlantas()
    {
        StopAllCoroutines();
        texto1Plantas.GetComponent<Animator>().SetBool("FadeTextBool", false);
        objeto1Plantas.GetComponent<Animator>().SetBool("FadeIcon", false);
        objeto1Plantas.SetActive(false);
        texto1Plantas.SetActive(false);
        texto2Plantas.GetComponent<Animator>().SetBool("FadeTextBool", false);
        objeto2Plantas.GetComponent<Animator>().SetBool("Resize", false);
        fondoPlantas.SetActive(false);
        texto2Plantas.SetActive(false);
        objeto2Plantas.SetActive(false);
        EnableGesturesPlantas(true);
    }

    //Cambia el titulo de la maqueta
    public void CambiaTitleMaqueta()
    {
        titleMaqueta.text = ModelManager.instance.desarrollos[curDesarrollo].nombre;
    }

    //Siguiente desarrollo dentro de la maqueta
    public void NextDesarrollo()
    {
        curDesarrollo++;
        if (curDesarrollo == ModelManager.instance.desarrollos.Count)
            curDesarrollo = 0;
        CambiaTitleMaqueta();
        ActiveBuild();
    }

    //PRevio desarrollo dentro de la maqueta
    public void PrevDesarrollo()
    {
        curDesarrollo--;
        if (curDesarrollo < 0)
            curDesarrollo = ModelManager.instance.desarrollos.Count - 1;
        CambiaTitleMaqueta();
        ActiveBuild();
    }

    public GameObject curModelActive;   //Contiene al objeto que es el modelo del desarrollo en el que se encuentra en ese momento

    //Proceso para asignar el actual desarrollo en la maqueta
    public void ActiveBuild()
    {
        foreach(Transform b in ModelManager.instance.builds.transform)
        {
            if(b.name == curDesarrollo.ToString())
            {
                b.gameObject.SetActive(true);
                curModelActive = b.gameObject;
            }
            else
                b.gameObject.SetActive(false);
        }
    }

    public GameObject popPupInterior;   //Contiene el popu para preguntar si se quiere ir a ver los interiores

    //Abre el popup interiores
    public void OpenPopUpInterior()
    {
        popPupInterior.SetActive(true);
        popPupInterior.GetComponent<Animator>().SetTrigger("CreceCeroUno");
    }

    //Cierra el popup interiores
    public void CerrarPopUpInterior()
    {
        popPupInterior.GetComponent<Animator>().SetTrigger("DecreceUnoCero");
    }

    //Cierra el popup interiores en pantalla sin RA
    public void CerrarPopUpInteriorWORA()
    {
        popPupInteriorWORA.GetComponent<Animator>().SetTrigger("DecreceUnoCero");
    }

    //*****Objetos correspondientes a la parte de experiencia 360
    public GameObject tuto360;
    public GameObject giroTuto;
    public GameObject touchTuto;
    public GameObject btnGyro;
    public GameObject btnTouch;
    public GameObject backMaqueta;
    public GameObject backWORA;
    //***********************************************************

    bool usingGiro; //Define si se esta usando giroscopio o touch

    //Lanza la experiencia 360
    public void Open360(bool open)
    {
        btnGyro.SetActive(false);
        btnTouch.SetActive(false);
        ModelManager.instance.Interiores.GetComponentInChildren<GyroController>().enabled = false;
        ModelManager.instance.Interiores.GetComponentInChildren<TouchController>().enabled = false;
        popPupInterior.GetComponent<Animator>().SetTrigger("DecreceUnoCero");
        StartCoroutine(open360(open));
        /*if (!open)
            comeFromWora = true;
        ModelManager.instance.RA.SetActive(!open);
        ModelManager.instance.Interiores.SetActive(open);
        backMaqueta.SetActive(open);
        backWORA.SetActive(!open);*/
    }

    //Abre la experiencia 360
    IEnumerator open360(bool open)
    {
        ModelManager.instance.SetParentBuild(ModelManager.instance.parentTemp);
        yield return new WaitForSeconds(0.1f);
        if (!open)
        {
            comeFrom360 = true;
            comeFromWora = true;
        }
        //ModelManager.instance.RA.SetActive(!open);
        ModelManager.instance.main.enabled = !open;
        ModelManager.instance.RA.enabled = !open;
        ModelManager.instance.Interiores.SetActive(open);
        backMaqueta.SetActive(open);
        backWORA.SetActive(!open);
    }

    //Abre la experiencia 360 sin RA
    public void Open360WORA(bool open)
    {
        btnGyro.SetActive(false);
        btnTouch.SetActive(false);
        ModelManager.instance.Interiores.GetComponentInChildren<GyroController>().enabled = false;
        ModelManager.instance.Interiores.GetComponentInChildren<TouchController>().enabled = false;
        popPupInteriorWORA.GetComponent<Animator>().SetTrigger("DecreceUnoCero");
        ModelManager.instance.parentModelWora.gameObject.SetActive(!open);
        ModelManager.instance.gestureWORA.transform.eulerAngles = new Vector3(0,0,0);
        ModelManager.instance.gestureWORA.SetActive(!open);
        ModelManager.instance.Interiores.SetActive(open);
        backWORA.SetActive(open);
        backMaqueta.SetActive(!open);
    }

    //Tuto de la experiencia 360
    public void RevisaTuto()
    {
        //if reproducir tuto
        StartCoroutine(ctuto360());
    }

    //Activa y desactiva los objetos del tuto 360 en el momento correspondiente
    IEnumerator ctuto360()
    {
        tuto360.SetActive(true);
        giroTuto.SetActive(true);
        giroTuto.transform.GetChild(0).GetComponent<Animator>().SetBool("FadeTextBool",true);
        giroTuto.transform.GetChild(2).GetComponent<Animator>().SetBool("FlechaBajaSube", true);
        yield return new WaitForSeconds(3);
        giroTuto.SetActive(false);
        touchTuto.SetActive(true);
        touchTuto.transform.GetChild(0).GetComponent<Animator>().SetBool("FadeTextBool", true);
        touchTuto.transform.GetChild(2).GetComponent<Animator>().SetBool("FlechaBajaSube", true);
        yield return new WaitForSeconds(3);
        TerminaTuto360();
    }

    //Finaliza tuto y empieza la experiencia
    public void TerminaTuto360()
    {
        giroTuto.SetActive(false);
        touchTuto.SetActive(false);
        ModelManager.instance.Interiores.GetComponentInChildren<TouchController>().enabled = true;
        btnGyro.SetActive(true);
        StopAllCoroutines();
        tuto360.SetActive(false);
    }

    //Cambio de giroscopio a touch y de touch a giroscopio
    public void ChangeController()
    {
        usingGiro = !usingGiro;
        ModelManager.instance.Interiores.GetComponentInChildren<GyroController>().enabled = usingGiro;
        ModelManager.instance.Interiores.GetComponentInChildren<TouchController>().enabled = !usingGiro;
        btnGyro.SetActive(!usingGiro);
        btnTouch.SetActive(usingGiro);
    }

    //********Objetos para la maqueta virtual sin RA
    public GameObject fondoWORA;
    public GameObject texto1wora;
    public GameObject texto2wora;
    public GameObject objeto1wora;
    public GameObject objeto2wora;
    public GameObject popPupInteriorWORA;
    public GameObject panelFullEtiquetaWORA;
    Coroutine tutoWora = null;
    public bool comeFromWora;
    public bool comeFrom360;
    //***********************************************

    //Abre WORA (Without RA)
    public void ProcesoStartWora()
    {
            ActiveBuild();
        StartCoroutine(procesoStartWORA());
    }

    //Cierra WORA
    public void ProcesoFinishWora()
    {
        StartCoroutine(procesoFinishWORA());
    }

    IEnumerator procesoStartWORA()
    {
        comeFrom360 = false;
        ModelManager.instance.SetParentBuild(ModelManager.instance.parentTemp);
        yield return new WaitForSeconds(0.1f);
        OpenWORA(true);
        yield return new WaitForSeconds(0.1f);
        StartTutoWORA();
    }

    IEnumerator procesoFinishWORA()
    {
        comeFromWora = true;
        CerrarMaquetaVirWORAYRegreso();
        yield return new WaitForSeconds(0.1f);
        OpenWORA(false);
        yield return new WaitForSeconds(0.1f);
        foreach(var lookat in curModelActive.transform.GetComponentsInChildren<LookAt>())
            lookat.SetTarget(ModelManager.instance.main.transform);

        //StopAllCoroutines();
    }

    //Activa el objeto para el funcionamiento del WORA
    public void OpenWORA(bool open)
    {
        //ModelManager.instance.RA.SetActive(!open);
        ModelManager.instance.main.enabled = !open;
        ModelManager.instance.RA.enabled = !open;
        ModelManager.instance.parentModelWora.gameObject.SetActive(open);
    }

    //Activa el tuto para la maqueta virtual sin RA (WORA)
    public void StartTutoWORA()
    {
        //btnWora.interactable = false;
        ModelManager.instance.SetParentBuild(ModelManager.instance.parentModelWora);
        foreach (var lookat in curModelActive.transform.GetComponentsInChildren<LookAt>())
            lookat.transform.GetComponentInChildren<LookAt>().SetTarget(ModelManager.instance.gestureWORA.GetComponentInChildren<Camera>().transform);
        ModelManager.instance.gestureWORA.SetActive(true);
        tutoWora = StartCoroutine(ContinuaTutoWORA());
    }

    //Muy parecido a la maqueta virtual con RA es el tuto
    IEnumerator ContinuaTutoWORA()
    {
        fondoWORA.SetActive(true);
        texto2wora.SetActive(false);
        texto1wora.SetActive(true);
        objeto2wora.SetActive(false);
        objeto1wora.SetActive(true);
        texto1wora.GetComponent<Animator>().SetBool("FadeTextBool", true);
        objeto1wora.GetComponent<Animator>().SetBool("Resize", true);
        yield return new WaitForSeconds(5);
        texto2wora.SetActive(true);
        texto1wora.SetActive(false);
        objeto2wora.SetActive(true);
        objeto1wora.SetActive(false);
        texto2wora.GetComponent<Animator>().SetBool("FadeTextBool", true);
        objeto2wora.GetComponent<Animator>().SetBool("Rotar", true);
        yield return new WaitForSeconds(5);
        texto2wora.SetActive(false);
        objeto2wora.SetActive(false);
        fondoWORA.SetActive(false);
    }

    //Cierra la maqueta virtual sin RA
    public void CerrarMaquetaVirWORA()
    {
        StopCoroutine(tutoWora);
        fondoWORA.SetActive(false);
        texto2wora.SetActive(false);
        texto1wora.SetActive(false);
        objeto2wora.SetActive(false);
        objeto1wora.SetActive(false);
        texto1wora.GetComponent<Animator>().SetBool("FadeTextBool", false);
        objeto1wora.GetComponent<Animator>().SetBool("Resize", false);
        texto2wora.GetComponent<Animator>().SetBool("FadeTextBool", false);
        objeto2wora.GetComponent<Animator>().SetBool("Rotar", false);
        
    }

    //Cierra y regresa a la pantalla con RA
    public void CerrarMaquetaVirWORAYRegreso()
    {
        CerrarMaquetaVirWORA();
        ModelManager.instance.SetParentBuild(ModelManager.instance.parentTemp);
        ModelManager.instance.gestureWORA.SetActive(false);
        OpenWORA(false);
    }

    //Abre popup de interiores en WORA
    public void OpenPopUpInteriorWORA()
    {
        popPupInteriorWORA.SetActive(true);
        popPupInteriorWORA.GetComponent<Animator>().SetTrigger("CreceCeroUno");
    }
    #endregion //public methods
}
