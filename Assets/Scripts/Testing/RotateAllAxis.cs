﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAllAxis : MonoBehaviour
{
    public float rotSpeed = 0.1f;

    //void Update()
    //{
    //    if (Input.GetMouseButton(0))
    //    {
    //        transform.Rotate(new Vector3(Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"), 0) * Time.deltaTime * rotSpeed);
    //    }

    //}

    private void OnMouseDrag()
    {
        float rotX = Input.GetAxis("Mouse X") * rotSpeed * Mathf.Deg2Rad;
        float rotY = Input.GetAxis("Mouse Y") * rotSpeed * Mathf.Deg2Rad;

        transform.RotateAround(Vector3.up, rotX);
        transform.RotateAround(Vector3.right, -rotY);
    }
}
