﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GesturePlantaBH : MonoBehaviour
{
    float initialFingersDistance;
    Vector3 initialScale;
    Vector3 initialPosition;

    public float rotatespeed = 0.1f;
    private float _startingPositionX;
    private float _startingPositionY;
    Touch touchTemp;
    bool paso;
    // Update is called once per frame
    void Update()
    {
        List<Touch> touches = InputHelper.GetTouches();

        //if (touches.Count == 1)
        //{
        //    Touch touch = touches[0];
        //    if (!paso)
        //    {
        //        switch (touch.phase)
        //        {
        //            case TouchPhase.Began:
        //                touchTemp = touch;
        //                paso = true;
        //                break;
        //        }
        //    }
        //    else
        //    {
        //        Touch touch2 = touches[0];
        //        switch (touch2.phase)
        //        {
        //            case TouchPhase.Began:
        //                initialFingersDistance = Vector2.Distance(touchTemp.position, touch2.position);
        //                initialScale = transform.localScale;
        //                break;
        //            case TouchPhase.Moved:
        //                var currentFingersDistance = Vector2.Distance(touchTemp.position, touch2.position);
        //                var scaleFactor = currentFingersDistance / initialFingersDistance;
        //                Vector3 newScale = new Vector3();
        //                newScale.x = Mathf.Clamp(initialScale.x * scaleFactor, 1, 2);
        //                newScale.y = Mathf.Clamp(initialScale.y * scaleFactor, 1, 2);
        //                newScale.z = Mathf.Clamp(initialScale.z * scaleFactor, 1, 1);
        //                transform.localScale = newScale;
        //                break;
        //            case TouchPhase.Ended:
        //                Debug.Log("Touch Phase Ended.");
        //                break;
        //        }
        //    }
        //}






        if (touches.Count == 1 && (transform.localScale.x > 1 || transform.localScale.y > 1))
        {
            Touch touch = touches[0];
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    _startingPositionX = touch.position.x;
                    _startingPositionY = touch.position.y;
                    initialPosition = transform.localPosition;
                    break;
                case TouchPhase.Moved:
                    var currentFingersDistanceX = _startingPositionX - touch.position.x;
                    var currentFingersDistanceY = _startingPositionY - touch.position.y;
                    var translateFactorX = currentFingersDistanceX / _startingPositionX;
                    var translateFactorY = currentFingersDistanceY / _startingPositionY;
                    Vector3 newPosition = new Vector3();
                    newPosition.x = Mathf.Clamp(initialPosition.x + (translateFactorX * -500), -750, 750);
                    newPosition.y = Mathf.Clamp(initialPosition.y + (translateFactorY * -500), -910, 910);
                    transform.localPosition = newPosition;
                    //if (_startingPosition > touch.position.x)
                    //{
                    //    transform.Rotate(Vector3.up, rotatespeed * Time.deltaTime);
                    //}
                    //else if (_startingPosition < touch.position.x)
                    //{
                    //    transform.Rotate(Vector3.down, rotatespeed * Time.deltaTime);
                    //}
                    break;
                case TouchPhase.Ended:
                    Debug.Log("Touch Phase Ended.");
                    break;
            }
        }
        if (touches.Count == 2)
        {
            Touch touch = touches[0];
            Touch touch2 = touches[1];
            switch (touch2.phase)
            {
                case TouchPhase.Began:
                    initialFingersDistance = Vector2.Distance(touch.position, touch2.position);
                    initialScale = transform.localScale;
                    break;
                case TouchPhase.Moved:
                    var currentFingersDistance = Vector2.Distance(touch.position, touch2.position);
                    var scaleFactor = currentFingersDistance / initialFingersDistance;
                    Vector3 newScale = new Vector3();
                    newScale.x = Mathf.Clamp(initialScale.x * scaleFactor, 1, 3);
                    newScale.y = Mathf.Clamp(initialScale.y * scaleFactor, 1, 3);
                    newScale.z = Mathf.Clamp(initialScale.z * scaleFactor, 1, 1);
                    transform.localScale = newScale;
                    if (transform.localScale == Vector3.one)
                        transform.localPosition = Vector3.zero;
                    break;
                case TouchPhase.Ended:
                    Debug.Log("Touch Phase Ended.");
                    break;
            }
        }
    }
}
