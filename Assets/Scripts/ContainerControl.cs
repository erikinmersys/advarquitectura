﻿// ************************************************************************ 
// File Name:   ContainerControl.cs 
// Purpose:    	OnEnable behavior
// Project:		ADVArquitecture
// Author:      Erik Marmolejo Ensastegui
// Copyright: 	2020 Inmersys
// ************************************************************************ 

// ************************************************************************ 
// Imports 
// ************************************************************************ 
using UnityEngine;

public class ContainerControl : MonoBehaviour
{
    #region public vars
    public int indexBuild;
    #endregion //public vars

    #region private methods
    // Called on object enable
    private void OnEnable()
    {
        //if (!TransitionControl.instance.comeFromWora)
        //{
            ModelManager.instance.SetParentBuild(transform);
            TransitionControl.instance.ActiveBuild();
        TransitionControl.instance.OmitirTutoMaqueta2();
        if(!TransitionControl.instance.comeFrom360 && !TransitionControl.instance.comeFromWora)
            TransitionControl.instance.ContinuaTuto();
            //TransitionControl.instance.btnWora.interactable = true;
        //}
        //else
          //  TransitionControl.instance.comeFromWora = false;
    }

    //Called on object disable
    private void OnDisable()
    {
        //TransitionControl.instance.btnWora.interactable = false;
        bool apagar = true;
        foreach (var cont in ModelManager.instance.RA.transform.GetComponentsInChildren<ContainerControl>())
            if (cont.isActiveAndEnabled)
                apagar = false;
        if(apagar)
            ModelManager.instance.SetParentBuild(ModelManager.instance.parentTemp);
    }
    #endregion //private methods
}
