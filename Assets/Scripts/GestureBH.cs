﻿// ************************************************************************ 
// File Name:   GestureBH.cs 
// Purpose:    	Control gesture resize, rotate, etc
// Project:		ADVArquitecture
// Author:      Erik Marmolejo Ensastegui
// Copyright: 	2020 Inmersys
// ************************************************************************ 

// ************************************************************************ 
// Imports 
// ************************************************************************ 
using System.Collections.Generic;
using UnityEngine;

public class GestureBH : MonoBehaviour
{
    float initialFingersDistance;
    Vector3 initialScale;

    public float rotatespeed = 0.1f;
    private float _startingPosition;

    // Update is called once per frame
    void Update()
    {
        List<Touch> touches = InputHelper.GetTouches();
        if (touches.Count == 1)
        {
            /*Touch touch = touches[0];
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    _startingPosition = touch.position.x;
                    break;
                case TouchPhase.Moved:
                    Vector3 localAngle = transform.localEulerAngles;
                    localAngle.y -= rotatespeed * touch.deltaPosition.x;
                    transform.localEulerAngles = localAngle;
                    //if (_startingPosition > touch.position.x)
                    //{
                    //    transform.Rotate(Vector3.up, rotatespeed * Time.deltaTime);
                    //}
                    //else if (_startingPosition < touch.position.x)
                    //{
                    //    transform.Rotate(Vector3.down, rotatespeed * Time.deltaTime);
                    //}
                    break;
                case TouchPhase.Ended:
                    Debug.Log("Touch Phase Ended.");
                    break;
            }*/
        }
            if (touches.Count == 2)
            {
                Touch touch = touches[0];
                Touch touch2 = touches[1];
                switch (touch2.phase)
                {
                    case TouchPhase.Began:
                        initialFingersDistance = Vector2.Distance(touch.position, touch2.position);
                        initialScale = transform.localScale;
                        break;
                    case TouchPhase.Moved:
                        var currentFingersDistance = Vector2.Distance(touch.position, touch2.position);
                        var scaleFactor = currentFingersDistance / initialFingersDistance;
                        Vector3 newScale = new Vector3();
                        newScale.x = Mathf.Clamp(initialScale.x * scaleFactor, 1, 1.5f);
                        newScale.y = Mathf.Clamp(initialScale.y * scaleFactor, 1, 1.5f);
                        newScale.z = Mathf.Clamp(initialScale.z * scaleFactor, 1, 1.5f);
                        transform.localScale = newScale;
                        break;
                    case TouchPhase.Ended:
                        Debug.Log("Touch Phase Ended.");
                        break;
                }
            }
    }
}
