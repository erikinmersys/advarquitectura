﻿// ************************************************************************ 
// File Name:   Desarrollo.cs 
// Purpose:    	Prefab Desarrollo properties
// Project:		ADVArquitecture
// Author:      Erik Marmolejo Ensastegui
// Copyright: 	2020 Inmersys
// ************************************************************************ 

// ************************************************************************ 
// Imports 
// ************************************************************************ 
using UnityEngine.UI;
using TMPro;
using UnityEngine;

// ************************************************************************ 
// Class: Desarrollo
// ************************************************************************ 
public class Desarrollo : MonoBehaviour
{
    #region public vars
    public int index;
    public Image previeDesarrolloImg;
    public TextMeshProUGUI nViviendasTxt;
    public TextMeshProUGUI nombreTxt;
    public TextMeshProUGUI tipoTxt;
    #endregion //public vars

    #region public methods
    public void SelectDesarrollo()
    {
        TransitionControl.instance.GoSelectedDesarrollo(index);
    }
    #endregion
}
