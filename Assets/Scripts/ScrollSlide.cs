﻿using System;
using UnityEngine;

public class ScrollSlide : MonoBehaviour
{
    int index;
    public void SlideMoved(Vector2 valor)
    {
        if (valor.x >= 0 && valor.x < 0.125f)
            index = 0;
        else if(valor.x >= 0.125f && valor.x < 0.375f)
            index = 1;
        else if (valor.x >= 0.375f && valor.x < 0.625f)
            index = 2;
        else if (valor.x >= 0.625f && valor.x < 0.875f)
            index = 3;
        else if (valor.x >= 0.875f && valor.x <= 1)
            index = 4;
    }

    public void GoToCorrectSlide()
    {
        gameObject.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<RectTransform>().localPosition = new Vector3(index * -1024, 0, 0);
        gameObject.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<RectTransform>().SetTop(0);
        gameObject.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<RectTransform>().SetBottom(0);
    }
}
